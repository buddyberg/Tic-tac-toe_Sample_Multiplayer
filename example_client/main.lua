local cs = require '../cs'
local client = cs.client

client.enabled = true
client.start(arg[2] or '127.0.0.1:22122')

-- Client connects to server. It gets a unique `id` to identify it.
--
-- `client.share` represents the shared state that server can write to and any client can read from.
-- `client.home` represents the home for this client that only it can write to and only server can
-- read from. `client.id` is the `id` for this client (set once it connects).
--
-- Client can also send or receive individual messages to or from server.


local share = client.share -- Maps to `server.share` -- can read
local home = client.home -- Maps to `server.homes[id]` with our `id` -- can write


function client.connect() -- Called on connect from server
  print(client.id)
end

function client.disconnect() -- Called on disconnect from server
end

function client.receive(...) -- Called when server does `server.send(id, ...)` with our `id`
end


-- Client gets all Love events

function client.load()
  x,y=0,0
  
end

function client.update(dt)
end

function drawcell(x,y,t)
  if t==0 then return end
  if t==1 then love.graphics.line(80+(x-1)*50+5,40+(y-1)*50+5,80+x*50-5,40+y*50-5) 
               love.graphics.line(80+x*50-5,40+(y-1)*50+5,80+(x-1)*50+5,40+y*50-5) end
  if t==2 then love.graphics.ellipse("line",80+(x-1)*50+25,40+(y-1)*50+25,20,20) end
end

function client.draw()
    if client.connected then
        love.graphics.setColor(0.5,0.5,0.5)
        love.graphics.rectangle("fill",80,40,150,150)
        love.graphics.setColor(0.7,0.7,0.7)
        love.graphics.rectangle("fill",80+x*50,40+y*50,50,50)
        love.graphics.setColor(1,1,1)
        love.graphics.line(130,40,130,190)
        love.graphics.line(180,40,180,190)
        love.graphics.line(80,90,230,90)
        love.graphics.line(80,140,230,140)
        for y,r in ipairs(share.field) do
          for x,c in ipairs(r) do
            drawcell(x,y,c)
          end
        end

        -- Draw our ping
        love.graphics.print('ping: ' .. client.getPing(), 20, 20)
        love.graphics.print('id:' .. client.id,20,35)
        love.graphics.print('turn:'..share.turn,20,50)
    else
        love.graphics.print('not connected', 20, 20)
    end
end

function client.keypressed(key)
   if key == "escape" then
      love.event.quit()
   end
   if key == "left" then
     if x==0 then else x=x-1 end
   elseif key == "right" then
     if x==2 then else x=x+1 end
   elseif key == "up" then
     if not (y==0) then y=y-1 end
   elseif key == "down" then
     if not (y==2) then y=y+1 end
   end
   print(share.turn,key,player())
   if share.turn==player() and key == "space" then
     client.send(x+1,y+1)
     print('sent')
   end
end
function player()
  if share.player1==client.id then return 1 else return 2 end
end