local cs = require 'cs'
local server = cs.server

server.enabled = true
server.start(arg[2] or '22122') -- Port of server
clSet=0
-- Server has many clients connecting to it. Each client has a unique `id` to identify it.
--
-- `server.share` represents shared state that the server can write to and all clients can read
-- from. `server.homes[id]` each represents state that the server can read from and client with
-- that `id` can write to (clients can't see each other's homes). Thus the server gets data
-- from each client and combines them for all clients to see.
--
-- Server can also send or receive individual messages to or from any client.
server.maxClients=2

local share = server.share -- Maps to `client.share` -- can write
local homes = server.homes -- `homes[id]` maps to `client.home` for that `id` -- can read

function server.connect(id) -- Called on connect from client with `id`
    print('client ' .. id .. ' connected')
    clSet=clSet+1
    if clSet==1 then share.player1=id else share.player2=id end
end

function server.disconnect(id) -- Called on disconnect from client with `id`
    print('client ' .. id .. ' disconnected')
    clSet=clSet-1
    if share.player1 and share.player2 then if share.player1==id then server.kick(share.player2) share.player2=nil else server.kick(share.player1) share.player1=nil end end
end

function server.receive(id, mx, my) -- Called when client with `id` does `client.send(...)`
   print('client ' .. id .. ' sent message',mx,my)
   if share.field[my][mx]==0 then 
     share.field[my][mx]=share.turn
     if share.turn==1 then share.turn=2 else share.turn=1 end
   end
end


-- Server only gets `.load`, `.update`, `.quit` Love events (also `.lowmemory` and `.threaderror`
-- which are less commonly used)

function server.load()
  share.field={{0,0,0},
               {0,0,0},
               {0,0,0}}
  share.turn=1
end

function server.update(dt)
    
end
